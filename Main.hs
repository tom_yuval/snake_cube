{-# LANGUAGE TupleSections #-}

module Main where

import Data.Array (elems)

import Cube
import Snake (Solution, State(State), CoreState(Initial))
import Solve (solve)

side :: Int
side = 4

segments :: [Int]
segments = [3, 4, 4, 4, 2, 4, 2, 4,
            2, 2, 2, 2, 2, 2, 2, 2,
            2, 3, 2, 4, 3, 3, 2, 4,
            2, 3, 2, 2, 2, 2, 2, 3,
            2, 2, 2, 2, 4, 2, 4]

range = ((1, 1, 1), (side, side, side))

firstDir :: Dir
firstDir = (0, 0, 1)

firstPos :: [Pos]
firstPos = [(1, 1, 1), (1, 1, 2), (1, 2, 1), (1, 2, 2), (2, 2, 1), (2, 2, 2)]

solutions = solve .
            State range segments $
            Initial (map (, firstDir) firstPos)

printSolution :: Solution Pos -> IO ()
printSolution solution = do
    print "solution:"
    print (elems solution)

main = mapM_ printSolution solutions
