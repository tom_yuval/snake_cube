{-# LANGUAGE MultiParamTypeClasses,
             FlexibleInstances,
             DatatypeContexts #-}

module Snake where

import Data.Array (Ix, Array, listArray, inRange, (!), (//))

import Maybe
import Solve
import Pos

type Solution pos = Array pos Int

data (Dir pos) dir =>
    CoreState pos dir = Initial [(pos, dir)] |
                        Started { curPath :: Solution pos,
                                  nextIndex :: Int,
                                  curPos :: pos,
                                  lastDir :: dir }

data (Dir pos) dir =>
    State pos dir = State { range :: (pos, pos),
                            remainingSegments :: [Int],
                            core :: CoreState pos dir }

next :: (Ix pos, (Dir pos) dir) =>
    State pos dir -> NextStep (State pos dir) (Solution pos)
next (State _ [] (Started path _ _ _)) = Solution path
next (State range
            remainingSegments@(curSegment:nextSegments)
            (Started curPath nextIndex curPos lastDir)) =
    let length = curSegment - 1 in
    NextStates $
    values [fmap (\(path, pos) -> State range
                                        nextSegments
                                        (Started path
                                                 (nextIndex + length)
                                                 pos
                                                 dir))
                 (putSegment length
                             curPath
                             (go dir curPos)
                             nextIndex
                             range
                             dir) | dir <- perpendiculars lastDir]
next (State range
            segments@(firstSegment:nextSegments)
            (Initial initialPlacements)) =
    NextStates $
    values [fmap (\(path, pos) -> State range
                                        nextSegments
                                        (Started path
                                                 (firstSegment + 1)
                                                 pos
                                                 dir))
                 (putSegment firstSegment
                             (emptyPath range)
                             pos
                             1
                             range
                             dir) | (pos, dir) <- initialPlacements]

emptyPath :: Ix pos => (pos, pos) -> Solution pos
emptyPath = flip listArray $ repeat 0

putSegment :: (Ix pos, (Dir pos) dir) => Int ->
                                         Solution pos ->
                                         pos ->
                                         Int ->
                                         (pos, pos) ->
                                         dir ->
                                         Maybe (Solution pos, pos)
putSegment length curPath pos nextIndex range dir =
    if not (inRange range pos) || curPath!pos /= 0
    then Nothing
    else let newPath = curPath//[(pos, nextIndex)]
             newIndex = nextIndex + 1 in
         if length == 1
         then Just (newPath, pos)
         else putSegment (length - 1)
                         newPath
                         (go dir pos)
                         newIndex
                         range
                         dir

instance (Ix pos, (Dir pos) dir) =>
    (PartialSolution (Solution pos)) (State pos dir) where
    nextStep = next
