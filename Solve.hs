{-# LANGUAGE MultiParamTypeClasses, FunctionalDependencies #-}

module Solve where

data NextStep state solution = NextStates [state] | Solution solution

class (PartialSolution solution) state | state -> solution where
    nextStep :: state -> NextStep state solution

solve :: (PartialSolution solution) state => state -> [solution]
solve state = case nextStep state of
    Solution solution -> [solution]
    NextStates states -> concat . map solve $ states
