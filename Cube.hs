{-# LANGUAGE MultiParamTypeClasses, FlexibleInstances #-}

module Cube where

import Pos
import Snake

type Pos = (Int, Int, Int)

type Dir = (Int, Int, Int)

dot :: Cube.Dir -> Cube.Dir -> Int
dot (x, y, z) (x', y', z') = x * x' + y * y' + z * z'

instance (Pos.Dir Pos) Cube.Dir where
    go (x, y, z) (x', y', z') = (x + x', y + y', z + z')
    allDirs = [(1, 0, 0), (-1, 0, 0),
               (0, 1, 0), (0, -1, 0),
               (0, 0, 1), (0, 0, -1)]
    perpendiculars dir = filter ((== 0).(`dot` dir)) allDirs
