module Maybe where

values :: [Maybe a] -> [a]
values [] = []
values ((Just x):xs) = x:(values xs)
values (Nothing:xs) = values xs
