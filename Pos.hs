{-# LANGUAGE MultiParamTypeClasses, FunctionalDependencies #-}

module Pos where

class (Dir pos) dir | dir -> pos where
    go :: dir -> pos -> pos
    allDirs :: [dir]
    perpendiculars :: dir -> [dir]
